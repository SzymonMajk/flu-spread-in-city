import unittest
from app.simulation import Simulation
from app.core import RoutinesFactory, LocationsFactory, PeopleFactory
from tests.test_utils import RelocatorStub, LocationsFactoryStub

relocator = RelocatorStub()
locations = LocationsFactoryStub()

class SimulationTestSuit(unittest.TestCase):

	def test_simulation_run_for_one_hour(self):
		sim = Simulation(1, locations)

		sim.step(relocator)

		self.assertEqual(True, sim.finished())

	def test_simulation_run_for_more_than_twenty_four_hours(self):
		more_than_24 = 25
		sim = Simulation(more_than_24, locations)

		for i in range(more_than_24):
			self.assertEqual(False, sim.finished())
			sim.step(relocator)

		self.assertEqual(True, sim.finished())

	def test_simulation_has_correct_hour(self):
		more_than_24 = 25
		correct_hour = 1
		sim = Simulation(more_than_24, locations)

		for i in range(more_than_24):
			sim.step(relocator)

		self.assertEqual(correct_hour, sim.hour())

	def test_simulation_correctly_calculate_day_of_week(self):
		more_than_24 = 25
		sim = Simulation(more_than_24, locations)

		for i in range(more_than_24):
			sim.step(relocator)

		self.assertEqual(1, sim.day())

	def test_simulation_correctly_calculate_day_for_next_week(self):
		more_than_7 = 24 * 9
		tuesday = 2
		sim = Simulation(more_than_7, locations)

		for i in range(more_than_7):
			sim.step(relocator)

		self.assertEqual(tuesday, sim.day())

	def test_simulation_correctly_calculate_weeks(self):
		weekends = 24 * 7 * 2
		correct_weekends = 2

		sim = Simulation(weekends, locations)

		for i in range(weekends):
			sim.step(relocator)

		self.assertEqual(correct_weekends, sim.week())
