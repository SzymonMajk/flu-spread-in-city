from app.core import Location


class RoutinesFactoryStub:

	def prepare_working_routine(behaviour):
		return {}

	def prepare_weekend_routine(behaviour):
		return {}


class LocationsFactoryStub:

	def __init__(self):
		self.homes = [Location("home_1", "Home", 0.7)]
		self.works = [Location("work_1", "Work", 0.4)]
		self.services = [Location("service_1", "Service", 0.5)]
		self.recreations = [Location("recreation_1", "Recreation", 0.15)]

	def prepare_locations(self):
		return [self.homes[0].id, self.works[0].id, self.services[0].id, self.recreations[0].id]


class RelocatorStub:

	def change_locations(self, people_relocations, locations):
		pass