import unittest
from app.core import PeopleFactory, Location
from tests.test_utils import RoutinesFactoryStub


class LocationTestSuit(unittest.TestCase):

	def add_one_healthy_one_infected(self, location):
		healthy = PeopleFactory.create_healthy_person(1, "Normal", "Worker", RoutinesFactoryStub)
		ill = PeopleFactory.create_healthy_person(2, "Normal", "Worker", RoutinesFactoryStub)
		ill.get_infected(1.0)

		location.add_person(healthy)
		location.add_person(ill)

	def test_should_infect(self):
		location = Location(1, "Home", 2.0)
		self.add_one_healthy_one_infected(location)

		location.transmit_infection(1.0, 1.0)

		self.assertEqual(2, location.latent + location.infected)
