import unittest
import random
from app.core import PeopleFactory
from tests.test_utils import RoutinesFactoryStub


class PersonTestSuit(unittest.TestCase):

	def test_get_infected(self):
		person = PeopleFactory.create_healthy_person(1, "Normal", "Worker", RoutinesFactoryStub)

		person.get_infected(1.0)

		self.assertEqual("Latent", person.infection_state)

	def test_normal_latent_into_infected_period(self):
		person = PeopleFactory.create_healthy_person(1, "Normal", "Worker", RoutinesFactoryStub)
		person.infection_state = "Latent"

		for i in range(4 * 24):
			person.update()

		self.assertEqual("Infected", person.infection_state)

	def test_weak_latent_into_infected_period(self):
		person = PeopleFactory.create_healthy_person(1, "Weak", "Worker", RoutinesFactoryStub)
		person.infection_state = "Latent"

		for i in range(5 * 24):
			person.update()

		self.assertEqual("Infected", person.infection_state)

	def test_strong_latent_into_infected_period(self):
		person = PeopleFactory.create_healthy_person(1, "Strong", "Worker", RoutinesFactoryStub)
		person.infection_state = "Latent"

		for i in range(2 * 24):
			person.update()

		self.assertEqual("Infected", person.infection_state)

	def test_vacationer_latent_into_infected_period(self):
		person = PeopleFactory.create_healthy_person(1, "Vacationer", "Worker", RoutinesFactoryStub)
		person.infection_state = "Latent"

		for i in range(4 * 24):
			person.update()

		self.assertEqual("Infected", person.infection_state)

	def test_normal_infected_cure(self):
		person = PeopleFactory.create_healthy_person(1, "Normal", "Worker", RoutinesFactoryStub)
		person.infection_state = "Latent"

		for i in range(10 * 24):
			person.update()

		self.assertEqual("Resistant", person.infection_state)

	def test_weak_infected_cure(self):
		person = PeopleFactory.create_healthy_person(1, "Weak", "Worker", RoutinesFactoryStub)
		person.infection_state = "Latent"

		for i in range(12 * 24):
			person.update()

		self.assertEqual("Healthy", person.infection_state)

	def test_strong_infected_cure(self):
		person = PeopleFactory.create_healthy_person(1, "Strong", "Worker", RoutinesFactoryStub)
		person.infection_state = "Latent"

		for i in range(7 * 24):
			person.update()

		self.assertEqual("Resistant", person.infection_state)

	def test_vacationer_infected_cure(self):
		person = PeopleFactory.create_healthy_person(1, "Vacationer", "Worker", RoutinesFactoryStub)
		person.infection_state = "Latent"

		for i in range(11 * 24):
			person.update()

		self.assertEqual("Resistant", person.infection_state)

	def test_resistant_cannot_infect(self):
		person = PeopleFactory.create_healthy_person(1, "Normal", "Worker", RoutinesFactoryStub)
		person.infection_state = "Resistant"

		person.get_infected(1.0)

		self.assertEqual("Resistant", person.infection_state)

	def test_create_people_and_ten_simulation_steps(self):
		types = ["Weak", "Normal", "Strong", "Vacationer"]
		people = []
		for i in range(50000):
			people.append(PeopleFactory.create_healthy_person(i, random.choice(types), "Worker", RoutinesFactoryStub))

		for i in range(10 * 7):
			for person in people:
				person.update()
