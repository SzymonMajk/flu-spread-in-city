import unittest
from app.core import RoutinesFactory
from tests.test_utils import LocationsFactoryStub


class RoutinesTestSuit(unittest.TestCase):

	def test_worker_routines(self):
		routines_factory = RoutinesFactory(LocationsFactoryStub())
		places = LocationsFactoryStub().prepare_locations()

		routines = routines_factory.create_routine(list(zip(places, [8, 8, 1, 0])))
		
		self.assertEqual({1: "home_1", 2: "home_1", 3: "home_1", 4: "home_1", 5: "home_1", 6: "home_1",
						  7: "home_1", 8: "home_1", 9: "work_1", 10: "work_1", 11: "work_1", 12: "work_1", 
						  13: "work_1", 14: "work_1", 15: "work_1", 16: "work_1", 17: "service_1", 18: "home_1",
						  19: "home_1", 20: "home_1", 21: "home_1", 22: "home_1", 23: "home_1", 24: "home_1",}, 
						  routines)

	def test_weekend_worker(self):
		routines_factory = RoutinesFactory(LocationsFactoryStub())
		places = LocationsFactoryStub().prepare_locations()

		routines = routines_factory.create_routine(list(zip(places, [9, 0, 2, 10])))

		self.assertEqual({1: "home_1", 2: "home_1", 3: "home_1", 4: "home_1", 5: "home_1", 6: "home_1",
						  7: "home_1", 8: "home_1", 9: "home_1", 10: "service_1", 11: "service_1", 12: "recreation_1",
						  13: "recreation_1", 14: "recreation_1", 15: "recreation_1", 16: "recreation_1", 17: "recreation_1", 18: "recreation_1",
						  19: "recreation_1", 20: "recreation_1", 21: "recreation_1", 22: "home_1", 23: "home_1", 24: "home_1",}, 
						  routines)
