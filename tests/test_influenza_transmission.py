import unittest
import random
from app.core import TransmissionCalculator


class InfluenzaTransmissionTestSuit(unittest.TestCase):

	def test_no_transmission_when_no_people(self):
		calculator = TransmissionCalculator()

		probability = calculator.calculate_probability(1, 1, 0, 0, 0)

		self.assertEqual(0.0, probability)

	def test_no_transmission_when_zero_coefficients(self):
		calculator = TransmissionCalculator()

		probability = calculator.calculate_probability(0, 0, 5, 5, 20)

		self.assertEqual(0.0, probability)

	def test_half_chance_when_one_latent_and_maximal_coefficient(self):
		calculator = TransmissionCalculator()

		probability = calculator.calculate_probability(1, 0, 1, 0, 2)

		self.assertEqual(0.5, probability)

	def test_half_chance_when_one_infected_and_maximal_coefficient(self):
		calculator = TransmissionCalculator()

		probability = calculator.calculate_probability(0, 1, 0, 1, 2)

		self.assertEqual(0.5, probability)
