import matplotlib.pyplot as plt
from app.simulation import Simulation
from app.core import RoutinesFactory, LocationsFactory, PeopleFactory, Relocator
from argparse import ArgumentParser


def add_person(person, locations_factory):
	locations_factory.add_person(person)


def simulate(people_number, duration, r_1, r_2, infect_initially, visualization, time):
	locations_factory = LocationsFactory()
	routines_factory = RoutinesFactory(locations_factory)
	relocator = Relocator()

	sim = Simulation(duration, locations_factory, r_1, r_2, infect_initially)

	for i in range(1, people_number):
		add_person(PeopleFactory.create_healthy_random(i, routines_factory), locations_factory)

	sim.infect_initially()

	for i in range(duration):
		sim.step(relocator)

	fig, ax = plt.subplots()
	t, stats = sim.get_statistics()
	fig, ax = plt.subplots()

	xlabel = "time "
	ylabel = "number of "

	if time == "Days":
		xlabel += "(days)"
		t = [(x / 24.0) for x in t]
	elif time == "Weeks":
		xlabel += "(weeks)"
		t = [(x / (24.0 * 7)) for x in t]
	else:
		xlabel += "(hours)"


	if visualization == "Latent":
		ylabel += "latent"
		ax.plot(t, [i[0] for i in stats])
	elif visualization == "Infected":
		ylabel += "infected"
		ax.plot(t, [i[1] for i in stats])
	elif visualization == "Sum":
		ylabel += "latent and infected together"
		ax.plot(t, [i[0] + i[1] for i in stats])
	else:
		ylabel += "latent and infected separate"
		ax.plot(t, stats)
		ax.legend(["Latent", "Infected"])

	ax.set(xlabel=xlabel, ylabel=ylabel, title='Flu spread results')
	ax.grid()
