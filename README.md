# Flu spread in city

# Be sure to have nose package for visualization:

pip install nose

# then possible to run tests:

nosetests tests

# If you want to run GUI script be sure StreamLit is installed:

pip install streamlit
pip install matplotlib

# then just type:

streamlit run gui.py

# or use terminal version with

python3 cli.py