import random


class Person:

	def __init__(self, id, type, state, latent_duration, infection_duration, normal_routines, weekend_routines):
		self.id = id
		self.type = type
		self.infection_state = state
		self.latent_duration_hours = latent_duration * 24
		self.infection_duration_hours = infection_duration * 24
		self.normal_routines = normal_routines
		self.weekend_routines = weekend_routines
		self.infection_standing_hours = 0

	def get_infected(self, probability):
		chance = random.random()

		if chance <= probability and self.infection_state is "Healthy":
			self.infection_state = "Latent"

	'''Should be run every hour of simulation, so every simulation step'''
	def update(self):
		if self.infection_state is "Latent":
			if self.infection_standing_hours == self.latent_duration_hours:
				self.infection_state = "Infected"
				self.infection_standing_hours += 1
				return True
			else:
				self.infection_standing_hours += 1

		elif self.infection_state is "Infected":
			if self.infection_standing_hours == self.infection_duration_hours:
				if self.type is "Weak":
					self.infection_state = "Healthy"
					self.infection_standing_hours = 0
					return True
				else:
					self.infection_state = "Resistant"
					return True
			else:
				self.infection_standing_hours += 1

		return False

	def get_location(self, hour):
		return self.normal_routines[hour]

	def should_relocate(self, hour, weekend):
		hour = hour % 24 + 1
		next_hour = hour + 1

		if (next_hour == 25):
			next_hour = 1

		if weekend:
			if (self.weekend_routines[hour] != self.weekend_routines[next_hour]):
				return True
			else:
				return False
		else:
			if (self.normal_routines[hour] != self.normal_routines[next_hour]):
				return True
			else:
				return False

	def get_relocate_details(self, hour, weekend):
		hour = hour % 24 + 1
		next_hour = hour + 1

		if (next_hour == 25):
			next_hour = 1

		if weekend:
			return self, self.weekend_routines[hour], self.weekend_routines[next_hour]
		else:
			return self, self.normal_routines[hour], self.normal_routines[next_hour]
