from app.person import Person
import random


class PeopleFactory:

	def create_healthy_person(id, type, behaviour, routines_factory):

		if type is "Normal":
			return Person(id, "Normal", "Healthy", 3, 9,
				routines_factory.prepare_working_routine(behaviour),
				routines_factory.prepare_weekend_routine(behaviour))
		elif type is "Weak":
			return Person(id, "Weak", "Healthy", 4, 11,
				routines_factory.prepare_working_routine(behaviour),
				routines_factory.prepare_weekend_routine(behaviour))
		elif type is "Strong":
			return Person(id, "Strong", "Healthy", 1, 5,
				routines_factory.prepare_working_routine(behaviour),
				routines_factory.prepare_weekend_routine(behaviour))
		elif type is "Vacationer":
			return Person(id, "Vacationer", "Healthy", 3, 10,
				routines_factory.prepare_working_routine(behaviour),
				routines_factory.prepare_weekend_routine(behaviour))

	def create_healthy_random(id, routines_factory):
		types = ["Normal", "Weak", "Strong", "Vacationer"]
		behaviours = ["Worker", "Homebody", "Workaholic", "Unemployed"]
		return PeopleFactory.create_healthy_person(id, random.choice(types),
			random.choice(behaviours), routines_factory)


class RoutinesFactory:

	def __init__(self, location_factory):
		self.location_factory = location_factory

	def create_routine(self, routine):
		created_routine = {}
		counter = 1

		for (p, d) in routine:
			for h in range(d):
				created_routine[counter] = p
				counter += 1

		for h in range(24 - len(created_routine)):
			created_routine[counter] = routine[0][0]
			counter += 1

		return created_routine

	def prepare_working_routine(self, behaviour):
		durations = []
		places = self.location_factory.prepare_locations(True)

		if behaviour is "Worker":
			durations = [8, 8, random.randint(0, 2), 0]
		elif behaviour is "Homebody":
			durations = [random.randint(8, 10), 8, random.randint(3, 5), 0]
		elif behaviour is "Workaholic":
			durations = [7, random.randint(8, 10), random.randint(1, 3), 0]
		elif behaviour is "Unemployed":
			durations = [random.randint(5, 11), 0, random.randint(4, 6), random.randint(3, 5)]

		return self.create_routine(list(zip(places, durations)))

	def prepare_weekend_routine(self, behaviour):
		places = self.location_factory.prepare_locations()
		durations = [random.randint(7,9), 0, random.randint(1, 2), random.randint(6, 10)]
		return self.create_routine(list(zip(places, durations)))


class LocationsFactory():

	def add_home(self):
		homes_number = len(self.homes)
		self.homes.append(Location("home_" + str(homes_number + 1), "Home", 0.7))

	def add_work(self):
		works_number = len(self.works)
		self.works.append(Location("work_" + str(works_number + 1), "Work", 0.4))

	def add_service(self):
		services_number = len(self.services)
		self.services.append(Location("service_" + str(services_number + 1), "Service", 0.5))

	def add_service(self):
		recreations_number = len(self.recreations)
		self.recreations.append(Location("recreation_" + str(recreations_number + 1), "Recreation", 0.15))

	def __init__(self):
		self.homes = [Location("home_1", "Home", 0.7)]
		self.works = [Location("work_1", "Work", 0.4)]
		self.services = [Location("service_1", "Service", 0.5)]
		self.recreations = [Location("recreation_1", "Recreation", 0.15)]
		self.in_current_home = 0
		self.in_current_work = 0
		self.in_current_service = 0
		self.in_current_recreations = 0

	def prepare_locations(self, increase=False):
		if (increase):
			self.in_current_home += 1
			self.in_current_work += 1
			self.in_current_service += 1
			self.in_current_recreations += 1

			if (self.in_current_home >= 4):
				self.add_home()
				self.in_current_home = 0
			if (self.in_current_work >= 100):
				self.add_work()
				self.in_current_work = 0
			if (self.in_current_service >= 50):
				self.add_service()
				self.in_current_service = 0
			if (self.in_current_recreations >= 300):
				self.add_service()
				self.in_current_recreations = 0

		return [self.homes[len(self.homes) - 1].id, self.works[len(self.works) - 1].id,
			self.services[len(self.services) - 1].id, self.recreations[len(self.recreations) - 1].id]

	def add_person(self, person):
		home = int(person.get_location(1).split("_", 1)[1])
		self.homes[home - 1].add_person(person)


class Location:

	def __init__(self, id, type, transmission_parameter):
		self.id = id
		self.type = type
		self.transmission_parameter = transmission_parameter
		self.people = []
		self.latent = 0
		self.infected = 0

	def infect_initially(self, probability):
		for person in self.people:
			chance = random.random()
			if chance <= probability and person.infection_state == "Healthy":
				person.infection_state = "Latent"
				self.latent += 1

	def add_person(self, person):
		self.people.append(person)
		if person.infection_state == "Latent":
			self.latent += 1
		elif person.infection_state == "Infected":
			self.infected += 1

	def remove_person(self, person):
		to_delete = None

		for human in self.people:
			if human.id == person.id:
				to_delete = human
				break

		if to_delete is not None:
			self.people.remove(to_delete)
			if person.infection_state == "Latent":
				self.latent -= 1
			elif person.infection_state == "Infected":
				self.infected -= 1

	def transmit_infection(self, r_1, r_2):
		people_probability = TransmissionCalculator().calculate_probability(r_1, r_2, self.latent, self.infected, len(self.people))
		total_probability = self.transmission_parameter * people_probability

		for person in self.people:
			chance = random.random()
			if chance <= total_probability and person.infection_state == "Healthy":
				person.infection_state = "Latent"
				self.update_person_state(person)

	def update_person_state(self, person):
		if person.infection_state == "Healthy" or person.infection_state == "Resistant":
			self.infected -= 1
		elif person.infection_state == "Latent":
			self.latent += 1
		elif person.infection_state == "Infected":
			self.latent -= 1
			self.infected += 1


class Relocator:

	def find_location(self, location, locations):
		type, id = location.split("_")
		id = int(id)

		if type == "home":
			return locations.homes[id - 1]
		elif type == "work":
			return locations.works[id - 1]
		elif type == "service":
			return locations.services[id - 1]
		elif type == "recreation":
			return locations.recreations[id - 1]

	def change_locations(self, people_relocations, locations):
		for person, current, next in people_relocations:
			self.find_location(current, locations).remove_person(person)
			self.find_location(next, locations).add_person(person)


class TransmissionCalculator:

	def calculate_probability(self, r_1, r_2, latent, infected, all):
		if all <= 0:
			return 0.0
		else:
			return r_1 * (latent / all) + r_2 * (infected / all)
