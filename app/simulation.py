import matplotlib
import matplotlib.pyplot as plt
import numpy as np


class Simulation:

	midnight = 24
	days_number = 7
	sathurday = 6
	week_changed = False

	def __init__(self, duration, locations, r_1=0.5, r_2=0.1, initially=0.05):
		self.duration = duration
		self.r_1 = r_1
		self.r_2 = r_2
		self.initially = initially
		self.locations = locations
		self.hours = 0
		self.days = 0
		self.weeks = 0
		self.stats = []

	def finished(self):
		if self.duration == self.hours:
			self.hours = 0
			self.stats = []
			return True
		else:
			return False

	def is_midnight(self):
		return (self.hours % self.midnight) == 0

	def is_weekend(self):
		return (((self.days % self.sathurday) == 0) or ((self.days % self.days_number) == 0))

	def is_sunday(self):
		return (self.days % self.days_number) == 0

	def infect_initially(self):
		for home in self.locations.homes:
			home.infect_initially(self.initially)

	def step(self, relocator):
		self.hours += 1
		latent_number = 0
		infected_number = 0
		people_relocations = []

		for home in self.locations.homes:
			home.transmit_infection(self.r_1, self.r_2)
			latent_number += home.latent
			infected_number += home.infected

			for person in home.people:
				if person.update():
					home.update_person_state(person)
				if person.should_relocate(self.hours, self.is_weekend()):
					people_relocations.append(person.get_relocate_details(self.hours, self.is_weekend()))

		for home in self.locations.works:
			home.transmit_infection(self.r_1, self.r_2)
			latent_number += home.latent
			infected_number += home.infected

			for person in home.people:
				if person.update():
					home.update_person_state(person)
				if person.should_relocate(self.hours, self.is_weekend()):
					people_relocations.append(person.get_relocate_details(self.hours, self.is_weekend()))

		for home in self.locations.services:
			home.transmit_infection(self.r_1, self.r_2)
			latent_number += home.latent
			infected_number += home.infected

			for person in home.people:
				if person.update():
					home.update_person_state(person)
				if person.should_relocate(self.hours, self.is_weekend()):
					people_relocations.append(person.get_relocate_details(self.hours, self.is_weekend()))

		for home in self.locations.recreations:
			home.transmit_infection(self.r_1, self.r_2)
			latent_number += home.latent
			infected_number += home.infected

			for person in home.people:
				if person.update():
					home.update_person_state(person)
				if person.should_relocate(self.hours, self.is_weekend()):
					people_relocations.append(person.get_relocate_details(self.hours, self.is_weekend()))

		self.stats.append((latent_number, infected_number))
		relocator.change_locations(people_relocations, self.locations)

		if self.is_midnight():
			self.days += 1
			if self.is_sunday():
				self.weeks += 1

	def get_statistics(self):
		return np.arange(0, self.hours), self.stats

	def hour(self):
		return self.hours % self.midnight

	def day(self):
		return self.days % self.days_number

	def week(self):
		return self.weeks
