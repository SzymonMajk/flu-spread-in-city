import streamlit as st
import matplotlib.pyplot as plt
from ui import simulate
from app.simulation import Simulation
from app.core import RoutinesFactory, LocationsFactory, PeopleFactory


people_number = st.sidebar.slider('People number', min_value=0, max_value=10000, value=1000)
duration = st.sidebar.slider('Duration in hours', min_value=0, max_value=10000, value=7000)
r_1 = st.sidebar.slider("R 1", min_value=0.01, max_value=0.5, value=0.04, step = 0.01)
r_2 = st.sidebar.slider("R 2", min_value=0.01, max_value=0.5, value=0.02, step = 0.01)
infect_initially = st.sidebar.slider("Initially infection", min_value=0.001, max_value=0.5, value=0.001, step=0.001)
visualization = st.sidebar.radio('What to show', ('Sum', 'Both', 'Latent', 'Infected'))
time = st.sidebar.radio('Time scale', ('Hours', 'Days', 'Weeks'))
start = st.sidebar.button("Start")

if start:
	simulate(people_number, duration, r_1, r_2, infect_initially, visualization, time)
	st.pyplot()
