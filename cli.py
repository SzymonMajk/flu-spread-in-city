import matplotlib.pyplot as plt
from ui import simulate
from app.simulation import Simulation
from app.core import RoutinesFactory, LocationsFactory, PeopleFactory


people_number = 3000
duration = 2500
r_1 = 0.05
r_2 = 0.02
infect_initially = 0.001
visualization = "Both"
time = "Days"

simulate(people_number, duration, r_1, r_2, infect_initially, visualization, time)
plt.savefig("result.png")
